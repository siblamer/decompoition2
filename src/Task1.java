public class Task1 {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = 5;
        boolean isComprime = areCoprime(a,b,c);
        if (isComprime) {
            System.out.println(a + ", " + b + ", and " + c + " are coprime.");
        } else {
            System.out.println(a + ", " + b + ", and " + c + " are not coprime.");

        }
    }

    public static int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    private static int gcd(int a, int b, int c) {
        return gcd(gcd(a, b), c);
    }

    public static boolean areCoprime(int a, int b, int c) {
        return gcd(a, b, c) == 1;
    }
}