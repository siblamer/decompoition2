public class Task2 {
    public static String add(String num1, String num2) {
        if (num1.length() < num2.length()) {
            String temp = num1;
            num1 = num2;
            num2 = temp;
        }

        int n1 = num1.length();
        int n2 = num2.length();

        String result = "";
        int carry = 0;

        for (int i = 0; i < n2; i++) {
            int sum = (num1.charAt(n1 - 1 - i) - '0') + (num2.charAt(n2 - 1 - i) - '0') + carry;
            carry = sum / 10;
            result = (sum % 10) + result;
        }

        for (int i = n2; i < n1; i++) {
            int sum = (num1.charAt(n1 - 1 - i) - '0') + carry;
            carry = sum / 10;
            result = (sum % 10) + result;
        }


        if (carry > 0) {
            result = carry + result;
        }

        return result;
    }
    public static void main(String[] args) {
        String a = "123";
        String b = "123";

        String sum = add(a,b);
        System.out.println(sum);
    }


}
